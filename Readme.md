# Chrome native messaging addon test 


Inorder to test 

- clone the the repo

```
git clone --depth=1  https://tesseract-index@bitbucket.org/tesseract-index/chrome-native-messaging-test-riz.git && cd chrome-native-messaging-test-riz
```



- Run 

``` bash

./json2msg.js < test-working.json | go run main.go
./json2msg.js < test-not-working.json | go run main.go
```


